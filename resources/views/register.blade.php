<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <p><label for="firstName">First name:</label></p>
        <input type="text" name="firstName" id="">
        <p><label for="lastName">Last name:</label></p>
        <input type="text" name="lastName" id="">
        <p><label for="gender">Gender:</label></p>
        <input type="radio" name="gender" id="" value="Male"> Male<br>
        <input type="radio" name="gender" id="" value="Female"> Female<br>
        <input type="radio" name="gender" id="" value="Other"> Other
        <p><label for="nationality">Nationality:</label></p>
        <select name="natonality" id="">
            <option value="indonesian">Indonesian</option>
            <option value="korean">Korean</option>
            <option value="thailand">Thailand</option>
        </select>
        <p><label for="LanguageSpoken">Language Spoken:</label></p>
        <input type="checkbox" name="languageSpoken" id="" value="Bahasa Indonesia"> Bahasa Indoneia <br>
        <input type="checkbox" name="languageSpoken" id="" value="English"> English <br>
        <input type="checkbox" name="languageSpoken" id="" value="Other"> Other <br>
        <p><label for="bio">Bio:</label></p>
        <textarea name="bio" id="" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">
    </form>
    
</body>
</html>